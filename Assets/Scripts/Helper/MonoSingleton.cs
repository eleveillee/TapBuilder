﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    [HideInInspector]
    public static MonoSingleton<T> Instance;

    public void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

}
