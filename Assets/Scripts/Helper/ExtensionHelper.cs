﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Lean;

public static class ExtensionHelper
{
    public static bool IsTarget(this Collider2D collider2D, LeanFinger finger)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(finger.ScreenPosition);
        Vector2 touchPos = new Vector2(wp.x, wp.y);

        if (collider2D == null)
            return false;

        return collider2D.bounds.Contains(touchPos);

        /*if (XInput.Down)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(XInput.Position);
            Vector2 touchPos = new Vector2(wp.x, wp.y);
            
            return collider2D.bounds.Contains(touchPos);
        }*/


    }
}
