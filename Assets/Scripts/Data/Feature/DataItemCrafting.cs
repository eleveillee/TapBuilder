﻿using System;
using UnityEngine;

[Serializable]
public class DataItemCrafting : DataFeature
{
    [SerializeField]
    private string m_itemInput;

    [SerializeField]
    private string m_itemOutput;

    [SerializeField]
    private float m_time;

    public string ItemInput
    {
        get { return m_itemInput; }
        set { m_itemInput = value; }
    }

    public string ItemOutput
    {
        get { return m_itemOutput; }
        set { m_itemOutput = value; }
    }

    public float Time
    {
        get { return m_time; }
        set { m_time = value; }
    }
}
