﻿using System;
using UnityEngine;

[Serializable]
public class DataCommunication : DataFeature
{
    [SerializeField]
    private float m_strenght;

    [SerializeField]
    private float m_range = 50.0f;

    public float Strenght
    {
        get { return m_strenght; }
        set { m_strenght = value; }
    }

    public float Range
    {
        get { return m_range; }
        set { m_range = value; }
    }
}
