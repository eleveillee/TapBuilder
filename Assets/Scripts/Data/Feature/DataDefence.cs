﻿using System;
using UnityEngine;

[Serializable]
public class DataDefence : DataFeature
{
    [SerializeField]
    private float _cooldown = 5.0f;

    public float Cooldown
    {
        get { return _cooldown; }
        set { _cooldown = value; }
    }

    [SerializeField]
    private float _tapPower = 5.0f;

    public float TapPower
    {
        get { return _tapPower; }
        set { _tapPower = value; }
    }

}
