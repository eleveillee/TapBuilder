﻿using System;
using UnityEngine;

[Serializable]
public class DataItemProduction : DataFeature
{
    [SerializeField]
    private string m_itemId;

    [SerializeField]
    private float m_coolDown = 1.0f;

    public string ItemId
    {
        get { return m_itemId; }
        set { m_itemId = value; }
    }

    public float CoolDown
    {
        get { return m_coolDown; }
        set { m_coolDown = value; }
    }
}
