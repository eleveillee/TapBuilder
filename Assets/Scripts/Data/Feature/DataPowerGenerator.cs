﻿using System;
using UnityEngine;

[Serializable]
public class DataPowerGenerator : DataFeature
{
    [SerializeField]
    private float m_powerProduction = 0.1f;

    public float PowerProduction
    {
        get { return m_powerProduction; }
        set { m_powerProduction = value; }
    }
}
