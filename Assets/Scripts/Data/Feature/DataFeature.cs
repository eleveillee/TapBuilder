﻿using System;
using UnityEngine;

public enum FeatureTypeEnum
{
    Portal,
    PowerGenerator,
    PowerStorage,
    Communication,
    ItemProduction,
    ItemCrafting,
    Defence 
}

[Serializable]
public class DataFeature : ScriptableObject
{
    [SerializeField]
    protected string m_name;

    [SerializeField]
    protected FeatureTypeEnum m_featureType;

    [SerializeField]
    protected float m_basePowerConsumption;

    public string Name
    {
        get { return m_name; }
        set { m_name = value; }
    }

    public FeatureTypeEnum FeatureType
    {
        get { return m_featureType; }
        set { m_featureType = value; }
    }

    public float BasePowerConsumption
    {
        get { return m_basePowerConsumption; }
        set { m_basePowerConsumption = value; }
    }
}
