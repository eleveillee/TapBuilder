﻿using System;
using UnityEngine;

[Serializable]
public class DataPowerStorage : DataFeature
{
    [SerializeField]
    private float m_maxStorage = 5.0f;

    public float MaxStorage
    {
        get { return m_maxStorage; }
        set { m_maxStorage = value; }
    }
}
