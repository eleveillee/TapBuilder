﻿using System;
using UnityEngine;

public class Data
{
    [SerializeField]
    private string m_id;

    [SerializeField]
    private string m_name;

    public string Id
    {
        get { return m_id; }
        set { m_id = value; }
    }

    public string Name
    {
        get { return m_name; }
        set { m_name = value; }
    }

    public Data()
    {
        m_id = Guid.NewGuid().ToString(); // On construction, a new GUID will be set, 
        m_name = "ConfigNoName";
    } 
}
