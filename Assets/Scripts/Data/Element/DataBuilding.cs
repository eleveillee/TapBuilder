﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DataBuilding : Data
{
    [SerializeField]
    protected List<DataFeature> m_features = new List<DataFeature>();

    [SerializeField]
    private GameObject m_prefab;

    public GameObject Prefab
    {
        get { return m_prefab; }
        set { m_prefab = value; }
    }

    public List<DataFeature> Features
    {
        get { return m_features; }
        set { m_features = value; }
    }
}
