﻿using System;
using UnityEngine;
using System.Collections.Generic;
[Serializable]
public class DataItem : Data
{
    [SerializeField]
    private Sprite _sprite;


    public Sprite Sprite
    {
        get { return _sprite; }
        set { _sprite = value; }
    }
}
