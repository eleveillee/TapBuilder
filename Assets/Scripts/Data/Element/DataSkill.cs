﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[Serializable]
public class DataSkill : Data
{
    [SerializeField]
    public float BaseCooldown;
    public Image SkillImage;
    public Image DarkMask;

    
}
