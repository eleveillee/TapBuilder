﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DataGoal : Data
{
    [SerializeField]
    private GoalType _type;

    [SerializeField]
    private List<string> _values = new List<string>();

    public GoalType Type
    {
        get { return _type; }
        set { _type = value; }
    }

    public List<string> Values
    {
        get { return _values; }
        set { _values = value; }
    }
}

public enum GoalType
{
    BuildingLevel,
    KillMonsters
}
