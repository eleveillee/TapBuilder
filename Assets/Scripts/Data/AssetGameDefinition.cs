﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AssetGameDefinition : ScriptableObject
{
    [SerializeField]
    private List<DataBuilding> m_buildings;
    public List<DataBuilding> Buildings{get { return m_buildings; } set { m_buildings = value; }}
    
    [SerializeField]
    private List<DataItem> m_items;
    public List<DataItem> Items {get { return m_items; } set { m_items = value; }}

    [SerializeField]
    private List<DataGoal> _goals;
    public List<DataGoal> Goals { get { return _goals; } set { _goals = value; } }

    [SerializeField]
    private List<DataSkill> _skills;
    public List<DataSkill> Skills { get { return _skills; } set { _skills = value; } }

    public Data GetAssetById(string id)
    {
        foreach (DataBuilding building in m_buildings)
        {
            if (building.Id == id)
                return building;
        }
        
        foreach (DataItem item in m_items)
        {
            if (item.Id == id)
                return item;
        }

        foreach (DataGoal goal in _goals)
        {
            if (goal.Id == id)
                return goal;
        }

        foreach (DataSkill skill in _skills)
        {
            if (skill.Id == id)
                return skill;
        }

        return null;
    }

    public DataItem GetItemById(string id)
    {
        foreach (DataItem item in m_items)
        {
            if (item.Id == id)
                return item;
        }
        return null;
    }

    public DataBuilding GetbuildingById(string id)
    {
        foreach (DataBuilding building in m_buildings)
        {
            if (building.Id == id)
                return building;
        }
        return null;
    }

    public DataSkill GetSkillById(string id)
    {
        foreach (DataSkill skill in _skills)
        {
            if (skill.Id == id)
                return skill;
        }
        return null;
    }
}

