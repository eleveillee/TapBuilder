﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.ComponentModel;

enum EnumCategory
{
    Buildings,
    Items,
    Skills
}

public class GameDefEditor : EditorWindow
{
    private static readonly Color FOLDOUT_BACKGROUND_COLOR = new Color(0.5f, 0.5f, 1, 0.25f);

    public static readonly int MINI_BUTTON_SIZE = 20;
    /*public static readonly int MEDIUM_BUTTON_WIDTH = 40;
    public static readonly int LABEL_COLUMN_WIDTH_BY_INDENT = 10;
    public static readonly int LABEL_COLUMN_WIDTH = 150;
    public static readonly int X_SPACER = 2;*/

    private static AssetGameDefinition m_assetGameDefinition;
    private int _curElementIndex = 0;

    private Vector2 npcScrollView = new Vector2();
    private EnumCategory _selectedCategory = EnumCategory.Buildings;   

    /*DataMystery m_currentMystery;

    ScenarioData m_currentScenario;*/
    //private int m_currentMysteryIndex = 0;

    //private int m_currentPatternIndex;
    //private int m_currentConditionIndex;

    /*private List<BaseConditionData> m_openConditions;
    private List<ScenarioPatternData> m_openPatterns;
    private List<DataMystery> m_mysteries;*/
 
    [MenuItem("Tap Builder/GameDef Editor")]
    public static void ShowWindow()
    {
        UnityEngine.Object assetGameDef = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/AssetGameDefinition.asset");
        m_assetGameDefinition = assetGameDef as AssetGameDefinition;

        GetWindow(typeof(GameDefEditor));
    }

    void OnEnable()
    {
        CheckExistence();
    }

    void OnDestroy()
    {
        //SaveCurrentDatas();
    }

    void OnGUI()
    {
        if (m_assetGameDefinition == null)
            return;
        
        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical(GUILayout.Width(200));
        DrawLeftList();
        GUILayout.EndVertical();

        GUILayout.Space(20);

        GUILayout.BeginVertical();

        GUILayout.Space(10);

        DrawSelection();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        if (GUI.changed)
            EditorUtility.SetDirty(m_assetGameDefinition);

    }

    void DrawLeftList()
    {
        EditorGUILayout.Separator();

        EnumCategory prevCategory = _selectedCategory;
        _selectedCategory = (EnumCategory)EditorGUILayout.EnumPopup(new GUIContent(), _selectedCategory);
        if (_selectedCategory != prevCategory)
            _curElementIndex = 0;
        
        List<GUIContent> contents = new List<GUIContent>();
        switch (_selectedCategory)
        {
            case EnumCategory.Buildings:
                if (GUILayout.Button(new GUIContent("+")))
                {
                    m_assetGameDefinition.Buildings.Add(new DataBuilding());
                    _curElementIndex = m_assetGameDefinition.Buildings.Count - 1;
                }

                foreach (DataBuilding builing in m_assetGameDefinition.Buildings)
                {
                    contents.Add(new GUIContent(builing.Name));
                }
                break;

            case EnumCategory.Items:
                if (GUILayout.Button(new GUIContent("+")))
                {
                    m_assetGameDefinition.Items.Add(new DataItem());
                    _curElementIndex = m_assetGameDefinition.Items.Count - 1;
                }

                foreach (DataItem item in m_assetGameDefinition.Items)
                {
                    contents.Add(new GUIContent(item.Name, Texture2D.whiteTexture));
                }
                break;

            case EnumCategory.Skills:
                if (GUILayout.Button(new GUIContent("+")))
                {
                    m_assetGameDefinition.Skills.Add(new DataSkill());
                    _curElementIndex = m_assetGameDefinition.Skills.Count - 1;
                }

                foreach (DataSkill skill in m_assetGameDefinition.Skills)
                {
                    contents.Add(new GUIContent(skill.Name, Texture2D.whiteTexture));
                }
                break;
        }

        npcScrollView = GUILayout.BeginScrollView(npcScrollView, GUILayout.Height(Screen.height - 50));
        _curElementIndex = GUILayout.SelectionGrid(_curElementIndex, contents.ToArray(), 1);

        GUILayout.EndScrollView();
    }
    
    void DrawSelection()
    {
        DisplayIdAndCopy();

        switch (_selectedCategory)
        {
            case EnumCategory.Buildings:
                if (m_assetGameDefinition.Buildings.Count <= 0)
                    return;
                DrawBuilding(m_assetGameDefinition.Buildings[_curElementIndex]);
                break;
            case EnumCategory.Items:
                if (m_assetGameDefinition.Items.Count <= 0)
                    return;
                DrawItem(m_assetGameDefinition.Items[_curElementIndex]);
                break;
            case EnumCategory.Skills:
                if (m_assetGameDefinition.Skills.Count <= 0)
                    return;
                DrawSkill(m_assetGameDefinition.Skills[_curElementIndex]);
                break;
        }
    }

    void DisplayIdAndCopy()
    {
        string dataId = "id Missing";
        switch (_selectedCategory)
        {
            case EnumCategory.Buildings:
                dataId = m_assetGameDefinition.Buildings[_curElementIndex].Id;
                break;
            case EnumCategory.Items:
                dataId = m_assetGameDefinition.Items[_curElementIndex].Id;
                break;
            case EnumCategory.Skills:
                dataId = m_assetGameDefinition.Skills[_curElementIndex].Id;
                break;
        }

        GUILayout.BeginHorizontal();
        GUILayout.Label(new GUIContent("Id : " + dataId));
        if (GUILayout.Button(new GUIContent("Copy ID To Clipboard")))
            EditorGUIUtility.systemCopyBuffer = dataId; // Copy to clipboard attempt, kinda work
        GUILayout.EndHorizontal();

    }

    void DrawItem(DataItem item)
    {
        
    }

    void DrawSkill(DataSkill item)
    {

    }

    void DrawBuilding(DataBuilding building)
    {
        if (building == null)
        {
            GUILayout.Label("Null building");
            return;
        }

        bool isDeleteEnabled = false; // Delete will be enabled when needed until confirmation is added to prevent loss of data
        if (isDeleteEnabled && GUILayout.Button(new GUIContent("DELETE, WARNING INSTA")))
        {
            int buildingIndex = m_assetGameDefinition.Buildings.FindIndex(x => x.Id == building.Id); // Using a Lambda expression to find the building that has the right Id and remove it
            m_assetGameDefinition.Buildings.RemoveAt(buildingIndex);
            if (buildingIndex == m_assetGameDefinition.Buildings.Count)
                _curElementIndex--;
        }

        building.Name = GUILayout.TextField(building.Name, GUILayout.MaxWidth(200.0f));
        GUILayout.BeginHorizontal();
            GUILayout.Label("Features");
            if (GUILayout.Button(new GUIContent("+"), GUILayout.MaxWidth(MINI_BUTTON_SIZE)))
                building.Features.Add(null);
        GUILayout.EndHorizontal();

        if (building.Features.Count == 0) // todo: add an empty feature, add it when something dropped in
        {
            GUILayout.Space(20.0f);
            GUILayout.Label("No Features");
        }
         
        int indexToRemove = -1;
        for (int i = 0; i < building.Features.Count; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(20.0f);
            building.Features[i] = (DataFeature)EditorGUILayout.ObjectField(building.Features[i], typeof(DataFeature), false);

            if (GUILayout.Button(new GUIContent("-"), GUILayout.MaxWidth(MINI_BUTTON_SIZE)))
                indexToRemove = i;
            GUILayout.EndHorizontal();
        }
        if (indexToRemove != -1)
            building.Features.RemoveAt(indexToRemove);
    }

    private void CheckExistence()
    {
        if (m_assetGameDefinition == null)
        {
            UnityEngine.Object assetGameDef = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/AssetGameDefinition.asset");
            m_assetGameDefinition = assetGameDef as AssetGameDefinition;   
        }
        
    }
}
