﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class GoapAiEditor : EditorWindow
{
    private static AssetGameDefinition _assetGameDefinition;

    [SerializeField] GameObject _target;
    GoapAgent _agent;
    // Planning datas
    Queue<GoapAction> _plan;

    [MenuItem("Tap Builder/Goap AI Visualizer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(GoapAiEditor));
    }

    private void OnEnable()
    {
        /*EditorApplication.playmodeStateChanged = () =>
        {
            if (Application.isPlaying)
            {
                GetTarget();
            }
        };*/
        CheckDependencies();
    }

    static bool CheckDependencies()
    {
        UnityEngine.Object assetGameDef =
            AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/AssetGameDefinition.asset");
        _assetGameDefinition = assetGameDef as AssetGameDefinition;

        if (_assetGameDefinition == null)
            return false;

        return true;
    }

    void OnGUI()
    {
        GUILayout.Label("AI Visualizer");
        if (!CheckDependencies())
            return;

        EditorGUILayout.BeginVertical();

        if (GetTarget() == false)
        {
            GUILayout.Label("Drop a Igoap(Drone) into the Field for more informations");
            return;
        }
        

        DisplayActions();

        if (!Application.isPlaying)
        {
            GUILayout.Label("Press Play for more informations...");
            return;
        }

        DisplayGoal();
        DisplayPlan();

        EditorGUILayout.EndVertical();
    }

    void DisplayGoal()
    {
        GUILayout.Label("== > GOAL");


        //((DroneAI_GOAP)_target.GetComponent<Drone>().Brain)

        IGoap _igoap = _agent.DataProvider;
        if (_igoap == null)
        {
            GUILayout.Label("igoap null");
        }
        else
        {
            GUILayout.Label(_igoap.GoalState.GetDescription(_assetGameDefinition));
        }
    }

    void DisplayActions()
    {
        GoapAction[] _actions = _target.GetComponents<GoapAction>();

        GUILayout.Label("== > ACTIONS");
        foreach (GoapAction action in _actions)
        {
            GUILayout.Label("- " + action.ActionName);
            GUILayout.Label("   o " + action.GoalState.GetDescription(_assetGameDefinition));
        }
    }

    void DisplayPlan()
    {
        GUILayout.Label("== > PLAN");
        if (_plan == null)
        {
            GUILayout.Label("Plan not found");
            return;
        }

        foreach (GoapAction action in _plan)
        {
            GUILayout.Label("- " + action.ActionName);
            GUILayout.Label("   o " + action.GoalState.GetDescription(_assetGameDefinition));
        }
    }

    void OnPlanEvaluated(Queue<GoapAction> plan)
    {
        _plan = plan;
    }

    bool GetTarget()
    {
        GameObject newTarget = (GameObject) EditorGUILayout.ObjectField(_target, typeof(GameObject));
        
        if (newTarget == null)
            return false;

        if (newTarget != _target)
            _target = newTarget;

        if (_agent == null)
        {
            if (_target != null)
            {
                _agent = _target.GetComponent<GoapAgent>();
            }

            if (_agent == null)
                return false;
        }

        _agent.OnPlanEvaluated += OnPlanEvaluated;

        return true;
    }

    void OnDestroy()
    {
        if (_agent != null)
            _agent.OnPlanEvaluated -= OnPlanEvaluated;
    }
}