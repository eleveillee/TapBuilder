﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEditor.ProjectWindowCallback;

/// <summary>
/// A helper class for instantiating ScriptableObjects in the editor.
/// </summary>
public class ScriptableObjectFactory
{
	[MenuItem("Assets/Create/ScriptableObject")]
	public static void CreateScriptableObject()
	{
		var assembly = Assembly.Load(new AssemblyName("Assembly-CSharp"));

        // Get all classes derived from ScriptableObject
        var allScriptableObjects = (from t in assembly.GetTypes()
									where t.IsSubclassOf(typeof(ScriptableObject))
		                            select t).ToArray();

		// Show the selection window.
		ScriptableObjectWindow.Init(allScriptableObjects);
    }
}

/// <summary>
/// Scriptable object window.
/// </summary>
public class ScriptableObjectWindow : EditorWindow
{
    private int selectedIndex;
    private static string[] names;

    private static Type[] types;

    private static Type[] Types
    {
        get { return types; }
        set
        {
            types = value;
            names = types.Select(t => t.FullName).ToArray();
        }
    }

    public static void Init(Type[] scriptableObjects)
    {
        Types = scriptableObjects;

        var window = EditorWindow.GetWindow<ScriptableObjectWindow>(true, "Create a new ScriptableObject", true);
        window.ShowPopup();
    }

    public void OnGUI()
    {
        GUILayout.Label("ScriptableObject Class");
        selectedIndex = EditorGUILayout.Popup(selectedIndex, names);

        if (GUILayout.Button("Create"))
        {
            var asset = ScriptableObject.CreateInstance(types[selectedIndex]);
            ProjectWindowUtil.StartNameEditingIfProjectWindowExists(
                asset.GetInstanceID(),
                ScriptableObject.CreateInstance<EndNameEdit>(),
                string.Format("{0}.asset", names[selectedIndex]),
                AssetPreview.GetMiniThumbnail(asset),
                null);

            Close();
        }
    }
}

internal class EndNameEdit : EndNameEditAction
{
    public override void Action(int instanceId, string pathName, string resourceFile)
    {
        AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
    }
}