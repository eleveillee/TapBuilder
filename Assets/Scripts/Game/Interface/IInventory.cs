﻿using System;
using UnityEngine;
using System.Collections.Generic;


public interface IInventory
{
    Inventory Inventory { get; }
}
