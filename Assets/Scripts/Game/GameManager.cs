﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [HideInInspector] 
    public static GameManager Instance;

    [SerializeField]
    private AssetGameDefinition m_gameDefinition;
    
    public AssetGameDefinition GameDefinition
    {
        get { return m_gameDefinition; }
        set { m_gameDefinition = value; }
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        Power.Initialize();
        Player.Initialize();
        GoalSystem.Initialize();
        Market.Initialize();
        TaskSystem.Initialize();

        /*if (string.IsNullOrEmpty(PlayerPrefs.GetString("Save")))
        {
            m_level = new Level();
        }
        else
        {
            SaveSystem.Load();
        }*/
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F9))
        {
            foreach (var key in Portal.Instance.Inventory.ItemList.Keys.ToList())
            {
                Portal.Instance.Inventory.ItemList[key] = Portal.Instance.Inventory.ItemList[key] + 100;
            }

            //Inventory.ItemList = Inventory.ItemList.ToDictionary(p => p.Key, p => p.Value + 100);

            /*foreach (KeyValuePair<string, int> pair in Inventory.ItemList)
            {
                //Inventory.ItemList[pair.Key] += 100;
            }*/
        }
        /*if (Input.GetKeyDown(KeyCode.F5))
        {
            SaveSystem.Save();
        }
        else if (Input.GetKeyDown(KeyCode.F6))
        {
            SaveSystem.Load();
        }*/
        //Clock.Update();
        Power.GameUpdate();
        TaskSystem.GameUpdate();
    }
}
