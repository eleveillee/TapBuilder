﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MonsterView : MonoBehaviour
{
    MonsterAI _monster;
    Sequence mySequence;

    void Start()
    {
        _monster = GetComponentInParent<MonsterAI>();
        _monster.OnTap += OnTap;
    }

    void Tween()
    {
        mySequence.Append(transform.DOScale(275f, 0.03f));
        mySequence.Append(transform.DOScale(25f, 0.08f));
    }

    public void OnTap()
    {
        StartCoroutine(TweenAndDie());
    }

    IEnumerator TweenAndDie()
    {
        Tween();
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject.transform.parent.gameObject); // View shouldn't be responsible for destroy the whole object, maybe split the body while it's dieing
    }
}
