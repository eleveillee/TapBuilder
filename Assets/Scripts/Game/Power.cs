﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;


static class Power
{
    [SerializeField]
    static private float _powerGeneration;

    [SerializeField]
    static private float _curStorage;

    [SerializeField]
    static private float _maxStorage = 25;

    static private List<PowerGenerator> _generators = new List<PowerGenerator>();
    static private List<PowerStorage> _storages = new List<PowerStorage>();

    static public void Initialize()
    {
       
    }

    public static void GameUpdate()
    {
        _maxStorage = GetMaxStorage();
        //_powerGeneration = _generators.Sum(x => ((DataPowerGenerator) x.Data).PowerProduction);
        _powerGeneration = 0;
        foreach (PowerGenerator gen in _generators)
        {
            if (!gen.Building.IsWorking)
                continue;

            _powerGeneration += ((DataPowerGenerator)gen.Data).PowerProduction * gen.Building.Level;
        }
        Add(_powerGeneration);
    }

    static float GetMaxStorage()
    {
        float powerStorage = 0.0f;
        foreach (PowerStorage storage in _storages)
        {
            if (!storage.Building.IsWorking)
                continue;

            powerStorage += ((DataPowerStorage)storage.Data).MaxStorage * storage.Building.Level;
        }

        return powerStorage;
    }


    // PowerGenerator are responsible for registering themselves. Might be a cleaner way to do it with callbacks.
    public static void UpdateGenerators(PowerGenerator generator, bool isAdded)
    {
        if(isAdded)
            _generators.Add(generator);
        else
            _generators.Remove(generator);

    }

    public static void UpdateStorages(PowerStorage generator, bool isAdded)
    {
        if (isAdded)
            _storages.Add(generator);
        else
            _storages.Remove(generator);

    }

    static public void Add(float value)
    {
        _curStorage += value;
        _curStorage = Mathf.Min(Mathf.Max(0f, _curStorage), _maxStorage);
    }

    public static float PowerGeneration
    {
        get { return _powerGeneration; }
        set { _powerGeneration = value; }
    }

    public static float CurStorage
    {
        get { return _curStorage; }
        set { _curStorage = value; }
    }

    public static float MaxStorage
    {
        get { return _maxStorage; }
        set { _maxStorage = value; }
    }
}
