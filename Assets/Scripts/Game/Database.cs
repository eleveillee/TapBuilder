﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Database
{
    public const string BUILDING_PORTAL  = "f23f4949-6ac6-4e41-8258-e87f54423a76";
    public const string BUILDING_BATTERY = "0d9ff657-886a-412a-9b37-3b1b8b69ea29";
    public const string BUILDING_ANTENNA = "781c6e7e-0530-4cf8-8ffd-6c0cfa68cf68";
    public const string BUILDING_QUARRY  = "a8369280-c249-4214-a016-937b8d2d0658";
    public const string BUILDING_FACTORY = "53586b0b-f430-4d23-95d4-563621ddce58";
    public const string BUILDING_URNACE  = "84a6921a-25a9-4ad5-b597-9e91c1ffc16a";
    
    public const string ITEM_IRONPLATE = "456a6ae3-30b7-4dfc-b79e-6fab85bfca71";
    public const string ITEM_GEAR      = "dab1462a-551f-4678-84e3-60fb15705418";
    public const string ITEM_IRONORE   = "ac00f08b-141a-440d-a3c4-1f8baa1838cf";
}