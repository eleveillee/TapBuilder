﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class GoalSystem
{
    public static void Initialize()
    {
        BuildingManager.Instance.OnBuildingLevelUp += OnBuildingLevelUp;
    }

    public static Goal GetNextGoal()
    {
        return new Goal(GameManager.Instance.GameDefinition.Goals[Player.GoalIndex]);
    }

    public static void CompleteGoal()
    {
        Debug.Log("Complete");
        Player.GoalIndex++;
        Player.CurGoal = GetNextGoal();
    }

    public static void OnBuildingLevelUp(Building building)
    {
        if (Player.CurGoal.Data.Type == GoalType.BuildingLevel && building.Data.Id == Player.CurGoal.Data.Values[0]  && building.Level >= int.Parse(Player.CurGoal.Data.Values[1]))
            CompleteGoal();
    }
}
