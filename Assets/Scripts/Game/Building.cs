﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Lean;

[Serializable]
public class Building : MonoBehaviour, IInventory //, ISerializable
{
    //[SerializeField, HideInInspector]
    protected DataBuilding _data;
    public Inventory Inventory { get; set; }

    protected Dictionary<DataFeature, Feature> m_features = new Dictionary<DataFeature, Feature>();

    protected bool _isWorking;

    public DataBuilding Data
    {
        get { return _data; }
    }

    private Collider2D _collider2D;
    private int _level = 1;
    private int _numberClicked;

    public Action OnTap; 
    public Action OnHold;
    public Action<bool> OnBuildingToggled;

    // Use this for initialization
    void Awake()
    {
        _collider2D = GetComponentInChildren<Collider2D>();
        Inventory = new Inventory();
    }
    
    protected virtual void OnEnable()
    {
        LeanTouch.OnFingerTap += OnFingerTap;
        LeanTouch.OnFingerHeldDown += OnFingerHold;
    }

    protected virtual void OnDisable()
    {
        LeanTouch.OnFingerTap -= OnFingerTap;
        LeanTouch.OnFingerHeldDown -= OnFingerHold; 
    }

    void OnFingerTap(LeanFinger finger)
    {
        if (_collider2D.IsTarget(finger))
            Tap();
    }
    
    void OnFingerHold(LeanFinger finger)
    {

        if ((OnTap != null) && _collider2D.IsTarget(finger))
            OnHold();
    }

    public void StartIni(DataBuilding data)
    {
        transform.name = "LocalBuilding(" + data.Name + ")";
        _data = data;
        SpawnFeatures();
        ToggleWorking(true);
    }
    
    public void Tap()
    {
        ToggleWorking(true);

        if(Power.CurStorage <= _level)
        Power.Add(-1f);
        AddClick(1);
        if (OnTap != null)
            OnTap();
    }

    void AddClick(int value)
    {
        _numberClicked += value;
        if (_numberClicked >= 10)
        {
            _level++;
            if(BuildingManager.Instance.OnBuildingLevelUp != null)
                BuildingManager.Instance.OnBuildingLevelUp(this);
            _numberClicked -= 10;
        }
    }

    void SpawnFeatures()
    {
        foreach (DataFeature feature in _data.Features)
        {
            this.AddFeature(gameObject, feature);
        }
    }

    public bool HasFeature(FeatureTypeEnum type)
    {
        return GetFeature(type) != null;
    }

    public Feature GetFeature(FeatureTypeEnum type)
    {
        foreach (KeyValuePair<DataFeature, Feature> feature in m_features)
        {
            if (feature.Key.FeatureType == type)
                return feature.Value;

        }
        return null;
    }
    
    public void ToggleWorking(bool isWorking)
    {
        _isWorking = isWorking;
        if(OnBuildingToggled != null)
            OnBuildingToggled(isWorking);
    }

    public void AddItem(Item item)
    {
        Inventory.AddItem(item);
        TaskSystem.AddTask(new Task(item));
    }

   

    #region Properties

    public Dictionary<DataFeature, Feature> Features
    {
        get { return m_features; }
        set { m_features = value; }
    }

    public bool IsWorking
    {
        get { return _isWorking; }
        set { _isWorking = value; }
    }

    public int Level
    {
        get { return _level; }
        set { _level = value; }
    }

    public int NumberClicked
    {
        get { return _numberClicked; }
        set { _numberClicked = value; }
    }
    #endregion

    /* // Serialization part, to be implemented soon
    public string Serialize()
    {
        PersistentData persData = new PersistentData(transform.position, Data.Id);

        return JsonUtility.ToJson(persData);
    }

    public void Deserialize(string json)
    {
        PersistentData persData = JsonUtility.FromJson<PersistentData>(json);
    }
    
    private class PersistentData
    {
        public Vector3 Position;
        public string Id;

        public PersistentData(Vector3 newPosition, string newId)
        {
            Position = newPosition;
            Id = newId;
        }
    }
    */
}