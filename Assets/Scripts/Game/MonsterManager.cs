﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MonsterManager : MonoBehaviour
{
    [HideInInspector]
    public static MonsterManager Instance;

    [SerializeField]
    private GameObject _monsterPrefab;

    private int _maxMonster = 6;

    private List<MonsterAI> _monsters = new List<MonsterAI>();

    void Awake()
    {
        if(Instance == null)
            Instance = this;
    }

    void Update()
    {
        if (_monsters.Count < _maxMonster && Random.Range(0f, 1f) <= 0.0035)
            SpawnMonster();
    }

    void SpawnMonster()
    {
        Vector2 target = new Vector2(540, 950);
        float minDistance = 1200.0f;
        Vector2 randPos = target + 2500 * Random.insideUnitCircle;
        float distance = Vector2.Distance(target,randPos);
        if (distance < minDistance)
            randPos *= minDistance / distance;

        Vector2 position = randPos;
        MonsterAI monsterAi = ((GameObject)Instantiate(_monsterPrefab, position, Quaternion.identity)).GetComponent<MonsterAI>();
        monsterAi.OnDie += OnMonsterDied;
        
        _monsters.Add(monsterAi);
    }
    
    public void OnMonsterDied(MonsterAI monster)
    {
        _monsters.Remove(monster);
    }

    public MonsterAI GetRandomMonster()
    {
        if (_monsters.Count > 0) 
            return _monsters[UnityEngine.Random.Range(0, _monsters.Count)];
        
        return null;

    }
    
    public List<MonsterAI> Monsters
    {
        get { return _monsters; }
    }
}