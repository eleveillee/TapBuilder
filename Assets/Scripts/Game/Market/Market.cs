﻿using System;
using UnityEngine;
using System.Collections.Generic;

using Random = UnityEngine.Random;

static class Market
{
    static private int _maxTransactions = 4;

    static List<Transaction> _transactions = new List<Transaction>();

    public static void Initialize()
    {
        for (int i = 0; i < _maxTransactions; i++)
        {
            AddRandomTransition();
        }
        
    }

    static Transaction GetRandomTransaction()
    {
        string itemId = GameManager.Instance.GameDefinition.Items[Random.Range(0, GameManager.Instance.GameDefinition.Items.Count)].Id;
        int number = Random.Range(1, 10);
        int price = Random.Range(1, 15);
        return new Transaction(itemId, number, price, 0.0f);
    }


    static public void AddRandomTransition()
    {
        AddTransaction(GetRandomTransaction());

    }

    static public void AddTransaction(Transaction transaction)
    {
        _transactions.Add(transaction);
        transaction.OnSell += OnTransactionSold;
    }

    public static void OnTransactionSold()
    {
        AddRandomTransition();
    }

    public static List<Transaction> Transactions
    {
        get { return _transactions; }
        set { _transactions = value; }
    }
}

/*
// Iron Plate
456a6ae3-30b7-4dfc-b79e-6fab85bfca71

// Gear 
dab1462a-551f-4678-84e3-60fb15705418

Iron : 
7d64d750-3d87-433d-ace0-74964bb8233d
*/
