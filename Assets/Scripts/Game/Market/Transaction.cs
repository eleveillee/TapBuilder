﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class Transaction
{
    private string _itemId;
    private int _quantity;
    private int _price;
    private float _duration;
    private float _startTime;
    public Action OnSell;

    public Transaction(string itemId, int quantity, int price, float duration)
    {
        _itemId = itemId;
        _quantity = quantity;
        _price = price;
        _duration = duration;
        _startTime = Time.realtimeSinceStartup; // To use a real time value, placeholder

    }

    public DataItem _dataItem;
    public DataItem DataItem
    {
        get
        {
            if(_dataItem == null)
                _dataItem = GameManager.Instance.GameDefinition.GetItemById(_itemId);
            
            return _dataItem;
        }
    }
    
    public string ItemId
    {
        get { return _itemId; }
        set { _itemId = value; }
    }

    public int Quantity
    {
        get { return _quantity; }
        set { _quantity = value; }
    }

    public int Price
    {
        get { return _price; }
        set { _price = value; }
    }
}

/*
// Iron Plate
456a6ae3-30b7-4dfc-b79e-6fab85bfca71

// Gear 
dab1462a-551f-4678-84e3-60fb15705418

Iron : 
7d64d750-3d87-433d-ace0-74964bb8233d
*/
