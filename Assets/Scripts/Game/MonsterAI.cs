﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;

public class MonsterAI : MonoBehaviour
{
    private Collider2D _collider2D;
    private Rigidbody2D _rigidbody2D;
    private float _speed;
    private Building _targetBuilding;
    private bool _isActive;
    public Action OnTap;
    public Action<MonsterAI> OnDie;

    private bool _isMoving = false;
    void Start()
    {
        _collider2D = GetComponentInChildren<Collider2D>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _speed = 500.0f + UnityEngine.Random.Range(0f, 1500f);
        _targetBuilding = BuildingManager.Instance.GetRandomBuilding();
        _isMoving = true;
        _isActive = true;
    }

    protected virtual void OnEnable()
    {
        LeanTouch.OnFingerTap += OnFingerTap;
    }

    protected virtual void OnDestroy()
    {
        LeanTouch.OnFingerTap -= OnFingerTap;
    }

    void OnFingerTap(LeanFinger finger)
    {
        if (_collider2D == null)
        {
            Debug.LogWarning("NullCollider");
            return;
        }

        if (_collider2D.IsTarget(finger))
            Tap();
    }

    void Update()
    {
        /*if (_isActive && _collider2D.IsUnder())
            DoClick();*/

    }

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        if (!_isMoving)
            return;

        Vector3 target = _targetBuilding.transform.position;
        //transform.position += 5.0f * (new Vector3(540, 950, 0) - transform.position).normalized;
        //_rigidbody2D.velocity = 250.0f * (new Vector3(540, 950, 0) - transform.position);
        _rigidbody2D.AddForce(_speed * (target - transform.position).normalized, ForceMode2D.Force); 
        //Debug.Log("Force : " +_speed * (_target - transform.position).normalized);
    }
    
    public void Tap()
    {
        if(OnTap != null)
            OnTap();

        Die();
        

    }
    
    void Die()
    {
        Portal.Instance.Inventory.AddGold(1);
        _isActive = false;

        if(OnDie != null)
            OnDie(this);
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!_isActive)
            return;

        Building building = collision.gameObject.GetComponentInParent<Building>();
        if (building == null)
            return;

        building.ToggleWorking(false);
        _rigidbody2D.velocity = Vector2.zero;
        _isMoving = false;
    }
}
