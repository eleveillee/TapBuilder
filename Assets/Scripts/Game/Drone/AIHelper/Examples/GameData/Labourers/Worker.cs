using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/**
 * A general labourer class.
 * You should subclass this for specific Labourer classes and implement
 * the createGoalState() method that will populate the goal for the GOAP
 * planner.
 */

public class Worker : IGoap
{
    //private GoapAgent _agent;
    public BaseDroneAI ActorAI { get; set; }

    private GoalState _goalState;
    public GoalState GoalState
    {
        get { return _goalState; }
    }

    public void Initialize(BaseDroneAI actor)
    {
        ActorAI = actor;
        _goalState = new GoalState(GoalStateType.HasItem, Database.ITEM_IRONORE);
    }
    
    void Update()
    {

    }

    /**
	 * Key-Value data that will feed the GOAP actions and system while planning.
	 */

    public HashSet<KeyValuePair<GoalState, object>> getWorldState()
    {
        HashSet<KeyValuePair<GoalState, object>> worldData = new HashSet<KeyValuePair<GoalState, object>>();

        //Todo Goalstate shoudl implement the condition itself
        //worldData.Add(new KeyValuePair<GoalState, object>(_goalState, Portal.Instance.Inventory.HasItem(Database.ITEM_IRONORE)));
        
        return worldData;
    }

    /**
	 * Implement in subclasses
	 */
    //public abstract HashSet<KeyValuePair<GoalState,object>> createGoalState ();

    public HashSet<KeyValuePair<GoalState, object>> createGoalState()
    {

        HashSet<KeyValuePair<GoalState, object>> goal = new HashSet<KeyValuePair<GoalState, object>>();

        goal.Add(new KeyValuePair<GoalState, object>(_goalState, true));
        return goal;
    }



	public void planFailed (HashSet<KeyValuePair<GoalState, object>> failedGoal)
	{
		// Not handling this here since we are making sure our goals will always succeed.
		// But normally you want to make sure the world state has changed before running
		// the same goal again, or else it will just fail.
	}

	public void planFound (HashSet<KeyValuePair<GoalState, object>> goal, Queue<GoapAction> actions)
	{
		// Yay we found a plan for our goal
		Debug.Log ("<color=green>Plan found</color> "+GoapAgent.prettyPrint(actions));
	}

	public void actionsFinished ()
	{
		// Everything is done, we completed our actions for this gool. Hooray!
		Debug.Log ("<color=blue>Actions completed</color>");
	}

	public void planAborted (GoapAction aborter)
	{
		// An action bailed out of the plan. State has been reset to plan again.
		// Take note of what happened and make sure if you run the same goal again
		// that it can succeed.
		Debug.Log ("<color=red>Plan Aborted</color> "+GoapAgent.prettyPrint(aborter));
	}

	public bool moveAgent(GoapAction nextAction)
	{
	    //Debug.Log("MoveAgent");
        ActorAI.Move(nextAction.target.transform.position);

        /*
        // move towards the NextAction's target
		float step = Time.deltaTime;
		gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, nextAction.target.transform.position, step);
		
		if (gameObject.transform.position.Equals(nextAction.target.transform.position) ) {
			// we are at the target location, we are done
			nextAction.setInRange(true);
			return true;
		} else
			return false;
            */
	    return false;
	}
}

