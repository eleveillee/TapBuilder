
using System;
using UnityEngine;
using System.Collections.Generic;

using Object = System.Object;
public class GoalState
{
    private GoalStateType _type;
    public List<Object> _datas = new List<Object>();

    public GoalState(GoalStateType type, Object o)
    {
        _type = type;
        _datas.Add(o);
    }

    public GoalStateType Type
    {
        get { return _type; }
        set { _type = value; }
    }

    public List<object> Datas
    {
        get { return _datas; }
        set { _datas = value; }
    }

    public string GetDescription(AssetGameDefinition assetGameDefinition)
    {
        string dataString = Type + " : ";
        for (int i = 0; i < Datas.Count; i++)
        {
            Data tryGetData = assetGameDefinition.GetAssetById(Datas[i].ToString());
            if (tryGetData != null)
                dataString += tryGetData.Name;
            else
                dataString += Datas[i].ToString();

            if (i < Datas.Count - 1)
                dataString += " - ";
        }

        
        return dataString;

    }

    public static bool operator ==(GoalState a, GoalState b)
    {
        Debug.Log(a + " vs "  + b);
        // If both are null, or both are same instance, return true.
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        // If one is null, but not both, return false.
        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        // Return true if the fields match:
        return a.Type == b.Type; // && a.Datas == b.Datas; // TODO Test for list too
    }

    public static bool operator !=(GoalState a, GoalState b)
    {
        return !(a == b);
    }
}

public enum GoalStateType
{
    HasItem,
    GetItem
}

