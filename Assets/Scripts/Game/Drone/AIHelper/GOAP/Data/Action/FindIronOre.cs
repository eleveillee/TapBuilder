
using System;
using UnityEngine;

public class FindIronOre : GoapAction
{

    private bool _isDebugVerbose = false;
    private IInventory targetInventory; // where we get the ore from
    
	public FindIronOre()
	{
	    _actionName = "FindIronOre";
        _goalState = new GoalState(GoalStateType.HasItem, Database.ITEM_IRONORE);
	    
        //addPrecondition (_goalState, false); // don't get a ore if we already have one
		addEffect (_goalState, true); // we now have a ore
	}
	
	
	public override void reset ()
	{
        targetInventory = null;
	}
	
	public override bool isDone ()
    {
        if (_isDebugVerbose)
            Debug.Log("IsDone : " + _actor.Inventory.HasItem(Database.ITEM_IRONORE));
        return _actor.Inventory.HasItem(Database.ITEM_IRONORE);
	}
	
	public override bool requiresInRange ()
	{
		return true; // yes we need to be near a supply pile so we can pick up the ore
	}
	
	public override bool checkProceduralPrecondition (GameObject agent)
    {
        float closestDistance = float.MaxValue;
	    Building closestBuilding = null;

	    foreach (Building building in BuildingManager.Instance.Buildings)
	    {
	        if (building.Inventory.HasItem(Database.ITEM_IRONORE))
	        {
	            float distance = Vector3.Distance(agent.transform.position, building.transform.position);
	            if (distance < closestDistance)
	            {
	                closestDistance = distance;
	                closestBuilding = building;
	            }
	        }

	    }
        if(_isDebugVerbose)
            Debug.Log("CheckProcedural ??");
        if (closestBuilding == null)
	        return false;

        if (_isDebugVerbose)
            Debug.Log("CheckProcedural !!");
        target = closestBuilding.gameObject;
	    targetInventory = closestBuilding;
	    return true;
	}
	
	public override bool perform (GameObject agent)
	{
		if (targetInventory.Inventory.ItemList[Database.ITEM_IRONORE] > 0)
		{
		    targetInventory.Inventory.RemoveItem(Database.ITEM_IRONORE);
			//TODO play effect, change actor icon
		    _actor.Inventory.AddItem(Database.ITEM_IRONORE);
            //BackpackComponent backpack = (BackpackComponent)agent.GetComponent(typeof(BackpackComponent));
            //backpack.numOre = 1;

		    if (_isDebugVerbose)
		        Debug.Log("PerformTrue");
                return true;
		} else {
            // we got there but there was no ore available! Someone got there first. Cannot perform action

            if (_isDebugVerbose)
                Debug.Log("PerformFalse");
            return false;
		}

	    return false;
	}
}

