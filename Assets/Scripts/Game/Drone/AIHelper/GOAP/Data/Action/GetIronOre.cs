
using System;
using UnityEngine;

public class GetIronOre : GoapAction
{
	private bool hasOre = false;
	private IInventory targetInventory; // where we get the ore from
	
	public GetIronOre() {

        GoalState goalState = new GoalState(GoalStateType.HasItem, Database.ITEM_IRONORE);
	    
        //addPrecondition (goalState, false); // don't get a ore if we already have one
		addEffect (goalState, true); // we now have a ore
	}
	
	
	public override void reset ()
	{
		hasOre = false;
        targetInventory = null;
	}
	
	public override bool isDone ()
	{
		return hasOre;
	}
	
	public override bool requiresInRange ()
	{
		return true; // yes we need to be near a supply pile so we can pick up the ore
	}
	
	public override bool checkProceduralPrecondition (GameObject agent)
    {
        Debug.Log("PickupIronOre - checkProceduralPrecondition");
        float closestDistance = float.MaxValue;
	    Building closestBuilding = null;

	    foreach (Building building in BuildingManager.Instance.Buildings)
	    {
	        if (building.Inventory.HasItem(Database.ITEM_IRONORE))
	        {
	            float distance = Vector3.Distance(agent.transform.position, building.transform.position);
	            if (distance < closestDistance)
	            {
	                closestDistance = distance;
	                closestBuilding = building;
	            }
	        }

	    }
        if (closestBuilding == null)
	        return false;
        
        target = closestBuilding.gameObject;
	    targetInventory = closestBuilding;
	    return true;
	}
	
	public override bool perform (GameObject agent)
	{
		if (targetInventory.Inventory.ItemList[Database.ITEM_IRONORE] > 0)
		{
		    targetInventory.Inventory.RemoveItem(Database.ITEM_IRONORE);
			hasOre = true;
			//TODO play effect, change actor icon
			//BackpackComponent backpack = (BackpackComponent)agent.GetComponent(typeof(BackpackComponent));
			//backpack.numOre = 1;
			
			return true;
		} else {
			// we got there but there was no ore available! Someone got there first. Cannot perform action
			return false;
		}

	    return false;
	}
}

