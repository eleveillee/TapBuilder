﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;
using DG.Tweening;

public class DroneController : BaseDroneController
{
    private bool _isMoving = false;
    
    public override void Move(Vector3 position)
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(_drone.transform.DOLocalMove(position, 1.0f));
        mySequence.OnComplete(TaskCompleted);
    }

    void TaskCompleted()
    {
    }
    
}
