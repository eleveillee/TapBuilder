﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;
using DG.Tweening;

public abstract class BaseDroneController // : IController
{
    protected Drone _drone;
    protected GameObject _target;

    public void InitializeAwakeBase(Drone drone)
    {
        _drone = drone;
        InitializeAwake();
    }

    public virtual void InitializeAwake() { }

    public abstract void Move(Vector3 position);
    
}
