﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;
using DG.Tweening;

public abstract class BaseDroneAI
{
    //private bool _isActive;
    protected bool _isBusy;
    protected Task _currentTask;
    protected Drone _actor;

    public abstract void SetTask(Task task);

    public void InitializeAwakeBase(Drone drone)
    {
        _actor = drone;
        InitializeAwake();
    }

    public virtual void InitializeAwake()
    {
    }

    public void Move(Vector3 position)
    {
        _actor.Controller.Move(position);
        Debug.Log("Move : " + position);
    }


    public bool IsBusy
    {
        get { return _isBusy; }
    }
}