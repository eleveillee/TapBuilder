﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Lean;
using UnityEngine;
using DG.Tweening;

public class Drone : MonoBehaviour
{
    private bool _isBusy;
    private Task _currentTask;

    private BaseDroneController _controller;
    private BaseDroneAI _brain;
    private Inventory _inventory;

    void Awake()
    {
        _controller = new DroneController();
        _controller.InitializeAwakeBase(this);

        _brain = new DroneAI_GOAP();
        _brain.InitializeAwakeBase(this);

        TaskSystem.AddDrone(this);
        _inventory = new Inventory();
    }

    public void SetTask(Task task)
    {
        _brain.SetTask(task);
    }

    void TaskCompleted()
    {
    }

    public BaseDroneController Controller
    {
        get { return _controller; }
    }

    public BaseDroneAI Brain
    {
        get { return _brain; }
    }

    public bool IsBusy
    {
        get { return _isBusy; }
    }

    public Inventory Inventory
    {
        get { return _inventory; }
    }
}
