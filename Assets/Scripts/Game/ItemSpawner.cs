﻿using UnityEngine;
using System.Collections.Generic;

public class ItemSpawner : MonoBehaviour
{
    [HideInInspector]
    public static ItemSpawner Instance;

    [SerializeField]
    private GameObject _itemPrefab;

    private List<Item> _items = new List<Item>();

    // Use this for initialization
    void Start () {

        if (Instance == null)
            Instance = this;

        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    
	// Update is called once per frame
	public Item SpawnItem (string itemId, Vector3 position)
	{

        GameObject go = (GameObject)Instantiate(_itemPrefab, position, Quaternion.identity);
	    Item item = go.GetComponent<Item>();
        _items.Add(item);

	    if (item == null)
	        return null;

	    item.Ini(GameManager.Instance.GameDefinition.GetItemById(itemId));
	    return item;
	}

    public List<Item> Items
    {
        get { return _items; }

    }


}
