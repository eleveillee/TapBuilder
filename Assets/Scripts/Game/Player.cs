﻿using System;
using UnityEngine;
using System.Collections.Generic;


static class Player
{
    static private int _level;

    static private int _goalIndex;
    static private Goal _curGoal;

    static public void Initialize()
    {
        _curGoal = GoalSystem.GetNextGoal();
    }

    public static int Level
    {
        get { return _level; }
        set { _level = value; }
    }

    public static Goal CurGoal
    {
        get { return _curGoal; }
        set { _curGoal = value; }
    }

    public static int GoalIndex
    {
        get { return _goalIndex; }
        set { _goalIndex = value; }
    }
}

   