﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class TaskSystem
{ 
    private static List<Task> _tasks = new List<Task>();
    private static List<Drone> _drones = new List<Drone>();

    public static bool IsAutomatic;

    public static void Initialize()
    {

    }

    public static void GameUpdate()
    {
        if (_tasks.Count <= 0)
            return;

        foreach (Drone drone in _drones)
        {
            if (!drone.IsBusy)
                drone.SetTask(_tasks[0]);

            _tasks.Remove(_tasks[0]);
        }
    }

    public static void AddTask(Task task)
    {
        _tasks.Add(task);
    }

    public static void AddDrone(Drone drone)
    {
        _drones.Add(drone);
    }

    public static List<Task> Tasks
    {
        get { return _tasks; }
        set { _tasks = value; }
    }
}
