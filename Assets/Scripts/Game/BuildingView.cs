﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

// Responsible for any visual for the building, sprites and UI
public class BuildingView : MonoBehaviour //, IPointerDownHandler, IDragHandler, IPointerUpHandler
{

    [SerializeField]
    UIBuildingView _uiBuildingViewPrefab;

    Building _building;
    SpriteRenderer _sprite;

    Sequence mySequence;

    void Awake()
    {
        _sprite = GetComponentInChildren<SpriteRenderer>();
        _building = GetComponentInParent<Building>();
    }

    void Start ()
    {
        mySequence = DOTween.Sequence();
        _uiBuildingViewPrefab.StartIni(_building);

        IniTween();
    }

    void OnEnable()
    {
        _building.OnTap += OnBuildingClicked;
        _building.OnBuildingToggled += OnBuildingToggled;
    }

    void Disable()
    {
        _building.OnTap += OnBuildingClicked;
        _building.OnBuildingToggled += OnBuildingToggled;
    }

    void IniTween()
    {
        mySequence.SetAutoKill(false);
        mySequence.Append(transform.DOScale(1.05f, 0.12f));
        mySequence.Append(transform.DOScale(1f, 0.05f));

        mySequence.Insert(0.0f, transform.DORotate(new Vector3(0.0f, 0.0f, 4.0f),0.08f));
        mySequence.Insert(0.08f, transform.DORotate(new Vector3(0.0f, 0.0f, -1.0f), 0.05f));
        mySequence.Insert(0.13f, transform.DORotate(new Vector3(0.0f, 0.0f, 0.0f), 0.04f));
        mySequence.Pause();

        /*
         * Sequence mySequence = DOTween.Sequence();
mySequence.Append(transform.DOMoveX(45, 1))
  .Append(transform.DORotate(new Vector3(0,180,0), 1))
  .PrependInterval(1)
  .Insert(0, transform.DOScale(new Vector3(3,3,3), mySequence.Duration()));
       */
    }

	// Update is called once per frame
	void OnBuildingClicked()
	{
        //Debug.Log(DOTween.tween);
	    _uiBuildingViewPrefab.UpdateInfo();
	    mySequence.Rewind();
        mySequence.Play();
	}

    void OnBuildingToggled(bool isWorking)
    {
        Color color;
        if (isWorking)
            color = Color.green;
        else
            color = Color.red;

        _sprite.color = color;
    }
}


// Test with PointerHandler but didn't work, I think it's just for UI not normal interactions
/*
 * public void OnPointerDown(PointerEventData data)
    {
        _textClicked.text = "Down";
        Debug.Log("FINGER DOWN");
        //prevPointWorldSpace = theCam.ScreenToWorldPoint(data.position);
    }

    public void OnDrag(PointerEventData data)
    {
        _textClicked.text = "Drag";
        Debug.Log("ONDrag : " + data);
        //thisPointWorldSpace = theCam.ScreenToWorldPoint(data.position);
        // realWorldTravel = thisPointWorldSpace - prevPointWorldSpace;
        //_processRealWorldtravel();
        // prevPointWorldSpace = thisPointWorldSpace;
    }

    public void OnPointerUp(PointerEventData data)
    {
        _textClicked.text = "Up";
        Debug.Log("clear finger...");
    }

    */
      