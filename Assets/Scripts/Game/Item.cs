﻿using System;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using Lean;

[Serializable]
public class Item : MonoBehaviour //, ISerializable
{
    private DataItem _data;

    [SerializeField]
    private SpriteRenderer _renderer;

    private Collider2D _collider2D;

    private void Start()
    {
        _collider2D = GetComponentInChildren<Collider2D>();
    }

    public void Ini(DataItem data)
    {
        _data = data;
        _renderer.sprite = data.Sprite;
    }

    public void GoTo(Vector3 position, Action<Item> OnComplete = null)
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOLocalMove(position, 1.0f));
        mySequence.OnComplete(() => OnGoToComplete(OnComplete)); // This is a fine piece of magic that I don't fully understand. But it works. Yeah!
    }

    void OnGoToComplete(Action<Item> OnComplete)
    {
        if (OnComplete != null)
            OnComplete.Invoke(this);
    }

    protected virtual void OnEnable()
    {
        LeanTouch.OnFingerTap += OnFingerTap;
    }

    protected virtual void OnDisable()
    {
        LeanTouch.OnFingerTap -= OnFingerTap;
    }

    void OnFingerTap(LeanFinger finger)
    {
        if (_collider2D.IsTarget(finger))
            Tap();
    }

    public void Tap()
    {
        Building portal = BuildingManager.Instance.GetBuilding(Database.BUILDING_PORTAL);
        if(portal != null)
            GoTo(portal.transform.position, OnReachPortal);
    }

    void OnReachPortal(Item item)
    {
        Portal.Instance.Inventory.AddItem(item.Data.Id);
        Destroy(item.gameObject);
    }

    void OnDestroy()
    {
        ItemSpawner.Instance.Items.Remove(this);
    }


    public DataItem Data
    {
        get { return _data; }
        set { _data = value; }
    }
}