﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillManager : MonoBehaviour
{
    [HideInInspector]
    public static SkillManager Instance;

        
    private List<DataSkill> _skills = new List<DataSkill>();

    private bool isOnCooldown = false;

    public bool IsOnCooldown
    {
        get { return isOnCooldown; }
        set { isOnCooldown = value; }
    }

    
    void Start()
    {
        Instance = this;
    }

    void Update()
    {
               
        
        if (!isOnCooldown)
        {
            UISkillButton.Instance.skillButton.enabled = true;

            if (Input.GetKeyDown(KeyCode.Alpha1))
                UseSkill1();
            else if (Input.GetKeyDown(KeyCode.Alpha2))
                UseSkill2();

            else return;

        }
        else
        {
            UISkillButton.Instance.skillButton.enabled = false;
            UISkillButton.Instance.OnCoolDown();
        }
    }

      
    //abstract void Globaltap(class type)
    //{
    //    type[] targets = FindObjectsOfType(typeof(type)) as type[];
    //    foreach (type ta in targets)
    //        ta.Tap();
    //}

    public void UseSkill1()
    {
        if (!isOnCooldown)
        {
            isOnCooldown = true;
            MonsterAI[] monsters = FindObjectsOfType(typeof(MonsterAI)) as MonsterAI[];
            foreach (MonsterAI mo in monsters)
                mo.Tap();
        }
        else return;
    }

    public void UseSkill2()
    {
        if (!isOnCooldown)
        {
            Building[] buildings = FindObjectsOfType(typeof(Building)) as Building[];
            foreach (Building bu in buildings)
                bu.Tap();
        }
        else return;
    }

    public void UseSkill3()
    {
        if (!isOnCooldown)
        {
            Item[] items = FindObjectsOfType(typeof(Item)) as Item[];
            foreach (Item it in items)
                it.Tap();
        }
        else return;
    }
}



