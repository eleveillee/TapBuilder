﻿using UnityEngine;
using System.Collections;
using Lean;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    private float orthoZoomSpeed = 200f;        // The rate of change of the orthographic size in orthographic mode.

    void Start()
    {
        LeanTouch.OnFingerDrag += OnFingerDrag;
    }

    void Update()
    {
        PinchToZoom();
    }

    void OnFingerDrag(LeanFinger finger)
    {
        Camera.main.transform.position -= (Vector3)finger.DeltaScreenPosition;
    }

    //To be updated when XInput support multitouch/scrollwheel
    void PinchToZoom()
    {
        // If there are two touches on the device...

        float d = Input.GetAxis("Mouse ScrollWheel");
        if (Mathf.Abs(d) > 0.05f)
        {
            UpdateCamera(-d);
        }
        else if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
            UpdateCamera(deltaMagnitudeDiff);
        }
    }

    void UpdateCamera(float deltaMagnitudeDiff)
    {
        Camera camera = Camera.main;
        // ... change the orthographic size based on the change in distance between the touches.
        camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

        // Make sure the orthographic size never drops below zero.
        camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);
    }
}