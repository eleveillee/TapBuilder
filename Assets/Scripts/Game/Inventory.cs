﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class Inventory
{
    private int _gold;

    Dictionary<string, int> _itemList = new Dictionary<string, int>();

    public Inventory()
    {
        // Should I fill it with empty values or TryContains everytime ??? To Evaluate.
        foreach (DataItem item in GameManager.Instance.GameDefinition.Items)
        {
            _itemList.Add(item.Id, 0);
        }
    }


    public void AddGold(int value)
    {
        _gold = Mathf.Max(0, _gold + value);
    }

    public void AddItem(Item item, int value = 1)
    {
        AddItem(item.Data.Id, value);
    }

    public void AddItem(string m_itemId, int value = 1)
    {
        if (!_itemList.ContainsKey(m_itemId))
        {
            if (GameManager.Instance.GameDefinition.GetAssetById(m_itemId) == null)
            {
                Debug.LogWarning("TRYING TO ADD AN INEXISTENT ITEM  id :" + m_itemId);
                return;
            }
            _itemList.Add(m_itemId, value);
        }
        else
            _itemList[m_itemId] = Mathf.Max(0, _itemList[m_itemId] + value);
    }

    public void RemoveItem(string m_itemId, int value = 1)
    {
        if (!_itemList.ContainsKey(m_itemId))
        {
            Debug.LogWarning("TRYING TO DELETE AN INEXISTENT ITEM  id :" + m_itemId);
            return;
        }

        _itemList[m_itemId] = Mathf.Max(0, _itemList[m_itemId] - value);
    }

    public bool HasItem(string m_itemId, int value = 1)
    {
        if (!_itemList.ContainsKey(m_itemId))
        {
            Debug.LogWarning("TRYING TO DELETE AN INEXISTENT ITEM  id :" + m_itemId);
            return false;
        }

        return _itemList[m_itemId] > value;
    }

    public Dictionary<string, int> ItemList
    {
        get { return _itemList; }
    }

    public int Gold
    {
        get { return _gold; }
        set { _gold = value; }
    }
}

/*
// Iron Plate
456a6ae3-30b7-4dfc-b79e-6fab85bfca71

// Gear 
dab1462a-551f-4678-84e3-60fb15705418

Iron : 
7d64d750-3d87-433d-ace0-74964bb8233d
*/