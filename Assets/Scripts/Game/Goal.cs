﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class Goal
{
    private DataGoal _data;

    public Goal(DataGoal data)
    {
        _data = data;
    }

    public DataGoal Data
    {
        get { return _data; }
    }
}

   