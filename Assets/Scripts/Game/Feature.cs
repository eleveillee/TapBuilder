﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class Feature : MonoBehaviour
{
    [SerializeField]
    protected DataFeature _dataFeature;

    protected Building _building;

    public Inventory Inventory
    {
        get { return _building.Inventory; }
    }

    public DataFeature Data
    {
        get { return _dataFeature; }
        set { _dataFeature = value; }
    }

    public Building Building
    {
        get { return _building; }
        set { _building = value; }
    }

    
    void Update()
    {
        if(_building.IsWorking)
            TickUpdate();
    }

    protected virtual void TickUpdate()
    {

    }
}
