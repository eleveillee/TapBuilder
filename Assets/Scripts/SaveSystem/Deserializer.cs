﻿using System;
using System.Collections.Generic;
using UnityEngine;

static class Deserializer
{
    public static SaveData Deserialize(string jsonSave)
    {
        return JsonUtility.FromJson<SaveData>(jsonSave);

        
    }

    public static string Serialize()
    {
        SaveData saveData = new SaveData();
        //saveData.LevelData = GameManager.Instance.Level.Serialize();

        return JsonUtility.ToJson(saveData);
    }
}
