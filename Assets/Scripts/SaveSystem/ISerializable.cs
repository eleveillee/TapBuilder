﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public interface ISerializable
{
    string Serialize();
    void Deserialize(string json);
}
