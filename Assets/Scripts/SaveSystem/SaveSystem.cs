﻿using System;
using System.Collections.Generic;
using UnityEngine;

static class SaveSystem
{
    public static string SaveInstance;

    public static void Save()
    {
        SaveInstance = Serializer.Serialize();
        PlayerPrefs.SetString("Save", SaveInstance);
        Debug.Log("SaveJson : " + SaveInstance);

    }

    public static void Load()
    {
        string jsonSave = PlayerPrefs.GetString("Save");
        SaveData saveData = Deserializer.Deserialize(jsonSave);
        //GameManager.Instance.Level.Deserialize(saveData.LevelData);
        /*foreach (Building b in saveData.Level.Buildings)
        {
            GameObject.Instantiate(b);
            Debug.Log(b.Data.Name + " / " + b.Data.Features.Count);
        }*/
    }
}

[Serializable]
public class SaveData
{
    public string LevelData;
    
}
