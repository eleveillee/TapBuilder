﻿using System;
using System.Collections.Generic;
using UnityEngine;

static class Serializer
{
    public static string Serialize()
    {
        SaveData saveData = new SaveData();
        //saveData.LevelData = GameManager.Instance.Level.Serialize();

        return JsonUtility.ToJson(saveData);
    }

}
