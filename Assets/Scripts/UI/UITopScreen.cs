﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Displayed UI for the building
public class UITopScreen : MonoBehaviour {

    [SerializeField]
    Text _powerValue;
    
    [SerializeField]
    Text _goldvalue;

    [SerializeField]
    Text _goal;

    void Update()
    {
        _powerValue.text = (int)Power.CurStorage + " / " + (int)Power.MaxStorage + "(" + Power.PowerGeneration + ")";
        _goldvalue.text = Portal.Instance.Inventory.Gold.ToString();
        if(Player.CurGoal != null)
            _goal.text = Player.CurGoal.Data.Name;
    }
}