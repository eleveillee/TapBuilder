﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UISkillButton : MonoBehaviour {

    [HideInInspector]
    public static UISkillButton Instance;

    public Image mask;//
    public float cooldownDuration;//Get this from DataSkill(GameManager)
    public Button skillButton;//

    private float nextReadyTime;
    private float cooldownTimeLeft;

    public Button SkillButton
    {
        get { return skillButton; }
        set { skillButton = value; }
    }

 
    //private void SpawnStartingSkill()
    //{
    //    SpawnSkill("88890767-1b9a-4991-aa1b-7f70b36cdfde", new Vector2(750f, 0f)); //Globaltap-Monsters
    //    SpawnSkill("d300a94a-2b19-4509-98bc-4cd623b1cb21", new Vector2(-750f, 0f)); //Globaltap-Buildings
    //    SpawnSkill("bd39d393-43bb-4dfb-87e0-ca9c1939b498", 750f * Vector2.up); //Globaltap-Items

    //}

    //void SpawnSkill(string skillId, Vector2 position)
    //{
    //    DataSkill data = GameManager.Instance.GameDefinition.GetSkillById(skillId);
    //    if (data == null)
    //    {
    //        Debug.LogWarning("Skill data missing for : " + skillId);
    //        return;
    //    }

    //    DataSkill skillButton = Instantiate(data, SkillSpawnPoints.transform.position, SkillSpawnPoints.transform.rotation) as DataSkill;

    //    skillButton.transform.SetParent(canvas.transform);
    //}

    public void SetCooldown()
    {
        nextReadyTime = cooldownDuration + Time.time;
        cooldownTimeLeft = cooldownDuration;
        mask.enabled = true;
    }

    public void OnCoolDown()
    {
        cooldownTimeLeft -= Time.deltaTime;
        mask.fillAmount = (cooldownTimeLeft / cooldownDuration);
    }

    void Update()
    {
        if (Time.time > nextReadyTime)
            SkillManager.Instance.IsOnCooldown = true;
    }

    
    void Start()
    {
        Instance = this; //replace by MonoSingleton
    }

    public void OnSkill1()
    {
        GetComponent<AudioSource>().Play();
        SkillManager.Instance.UseSkill1();

        SetCooldown();

    }

    public void OnSkill2()
    {
        GetComponent<AudioSource>().Play();
        SkillManager.Instance.UseSkill2();

        SetCooldown();
    }

    public void OnSkill3()
    {
        //GetComponent<AudioSource>().Play();
        SkillManager.Instance.UseSkill3();

       SetCooldown();
    }

        void Awake()
    {
        mask.fillAmount = 0;
        mask.enabled = false;
    }
}


