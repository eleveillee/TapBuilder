﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// Displayed UI for the building
public class UIButtons : MonoBehaviour {
    
    public void OnInventory()
    {
        UIManager.Instance.ToggleInventory();
    }

    public void OnMarket()
    {
        UIManager.Instance.ToggleMarket();
    }

    public void OnBuilding()
    {
        UIManager.Instance.ToggleInventory();
    }

   
}