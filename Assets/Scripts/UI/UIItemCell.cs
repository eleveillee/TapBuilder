﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Displayed UI for the building
public class UIItemCell : MonoBehaviour {


    [SerializeField]
    Image _itemVisual;

    [SerializeField]
    Text _itemName;
    
    [SerializeField]
    Text _itemValue;

    private DataItem _data;
    private bool _isInitialized;

    public void Ini(DataItem data)
    {
        _isInitialized = true;
        _data = data;
        _itemVisual.sprite = data.Sprite;
        _itemName.text = data.Name;
    }

    void Update()
    {
        if (!_isInitialized)
            return;

        _itemValue.text = Portal.Instance.Inventory.ItemList[_data.Id].ToString();

    }
}