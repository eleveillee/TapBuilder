﻿using UnityEngine;
using System.Collections;
using System.Reflection.Emit;
using Lean;
using UnityEngine.UI;

// Displayed UI for the building
public class UIPanelBuildingInfo : MonoBehaviour
{
    [SerializeField]
    private Text _buildingName;

    [SerializeField]
    private GameObject _rootPortal;

    protected Building _building;

    public void Ini(Building building)
    {
        Reset();
        _building = building;
        _buildingName.text = building.Data.Name;
        LeanTouch.OnFingerTap += OnTap;

        if (_building.Data.Id == Database.BUILDING_PORTAL)
        {
            _rootPortal.SetActive(true);
        }
    }

    void Reset()
    {
        _rootPortal.SetActive(false);
    }

    void OnDisable()
    {
        LeanTouch.OnFingerTap -= OnTap;
    }

    void OnTap(LeanFinger finger)
    {
        if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
            
    }
}