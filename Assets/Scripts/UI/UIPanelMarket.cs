﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

// Displayed UI for the building
public class UIPanelMarket : MonoBehaviour {

    [SerializeField]
    GameObject _itemCellPrefab;

    [SerializeField]
    List<UITransactionCell> _cells = new List<UITransactionCell>();
    public void Start()
    {
        IniVisual();
    }
    
    // Reinstantiate all cell. To be updated with a better system
    public void IniVisual()
    {
        int counter = 0;
        foreach (Transaction transaction in Market.Transactions)
        {
            GameObject go = (GameObject)Instantiate(_itemCellPrefab);
            go.transform.SetParent(transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = new Vector3(-275f, 250 + -80f * counter, 0f); // Hardcoded values should be replaced to something more dynamic


            UITransactionCell uiCell = go.GetComponent<UITransactionCell>();
            _cells.Add(uiCell);
            uiCell.Ini(transaction);
            uiCell.OnSell += UpdateVisual;
            counter++;
        }
    }

    void UpdateVisual()
    {
        Clear();
        IniVisual();
    }

    void Clear()
    {
        foreach (UITransactionCell cell in _cells)
        {
            Destroy(cell.gameObject);
        }

        _cells.Clear();
    }
}