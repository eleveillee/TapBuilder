﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Displayed UI for the building
public class UITransactionCell : MonoBehaviour {
    

    [SerializeField]
    Image _itemVisual;

    [SerializeField]
    Text _itemName;

    [SerializeField]
    Text _itemValue;
    
    [SerializeField]
    Text _itemPrice;

    public Action OnSell;

    private Transaction _transaction;
    private bool _isInitialized;

    public void Ini(Transaction transaction)
    {
        if (transaction.DataItem == null)
        {
            Debug.LogWarning("Empty Transaction Data Item");
            return;
        }

        _transaction = transaction;
        _itemVisual.sprite = transaction.DataItem.Sprite;
        _itemName.text = transaction.DataItem.Name;
        _itemValue.text = transaction.Quantity.ToString();
        _itemPrice.text = transaction.Price + "$";

    }

    public void Sell()
    {
        if (Portal.Instance.Inventory.ItemList[_transaction.DataItem.Id] < _transaction.Quantity)
            return;

        Portal.Instance.Inventory.RemoveItem(_transaction.DataItem.Id, _transaction.Quantity);
        Portal.Instance.Inventory.AddGold(_transaction.Price);
        Market.Transactions.Remove(_transaction);
        _transaction.OnSell();
        OnSell();
        
    }

}