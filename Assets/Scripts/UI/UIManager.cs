﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// Displayed UI for the building
public class UIManager : MonoBehaviour
{

    public static UIManager Instance;

    
    [SerializeField]
    UIPanelInventory _uiInventory;

    [SerializeField]
    UIPanelMarket _uiMarket;

    //[SerializeField]
    //UIPanelSpellBook _uiSpellBook;

    void Awake()
    {
        Instance = this;
    }

    public void OpenInventory(bool enabled)
    {
        _uiInventory.gameObject.SetActive(enabled);
        if(enabled)
            _uiInventory.IniVisual();
    }

    public void ToggleInventory()
    {
        OpenInventory(!_uiInventory.isActiveAndEnabled);
    }

    public void OpenMarket(bool enabled)
    {
        _uiMarket.gameObject.SetActive(enabled);
        if (enabled)
            _uiMarket.IniVisual();
    }

    public void ToggleMarket()
    {
        OpenMarket(!_uiMarket.isActiveAndEnabled);
    }

    //public void OpenSpellBook(bool enabled)
    //{
    //    _uiSpellBook.gameObject.SetActive(enabled);
    //    if (enabled)
    //        _uiSpellBook.IniVisual();
    //}

    //public void ToggleSpellBook()
    //{
    //    OpenSpellBook(!_uiSpellBook.isActiveAndEnabled);
    //}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
            ToggleInventory();

        if (Input.GetKeyDown(KeyCode.F2))
            ToggleMarket();

        if (Input.GetKeyDown(KeyCode.F3))
            TaskSystem.IsAutomatic = !TaskSystem.IsAutomatic;

    }

    public UIPanelInventory Inventory
    {
        get { return _uiInventory; }
        set { _uiInventory = value; }
    }    
}