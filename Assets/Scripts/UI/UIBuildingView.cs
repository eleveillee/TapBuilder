﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Displayed UI for the building
public class UIBuildingView : MonoBehaviour {

    [SerializeField]
    Text _labelName;
    
    [SerializeField]
    Text _labelLevel;
    
    [SerializeField]
    Text _labelClick;
    
    [SerializeField]
    UIPanelBuildingInfo _uiPanelInfoPrefab;

    [SerializeField]
    static UIPanelBuildingInfo _panelBuildingInstance;

    private Building _building;
    public void StartIni(Building building)
    {
        _building = building;
        _labelName.text = _building.Data.Name;
        _building.OnHold += OnBuildingHold;
        //UpdateInfo();
    }

    /*void OnEnable()
    {
        _building.OnHold += OnBuildingHold;
    }*/

    void OnDisable()
    {
        _building.OnHold -= OnBuildingHold;
    }

    void OnBuildingHold()
    {
        if (_panelBuildingInstance != null)
            Destroy(_panelBuildingInstance.gameObject);
        
        _panelBuildingInstance = Instantiate(_uiPanelInfoPrefab, transform.position, Quaternion.identity) as UIPanelBuildingInfo;
        _panelBuildingInstance.Ini(_building);
    }

    public void UpdateInfo()
    {
        _labelClick.text = _building.NumberClicked.ToString();
        _labelLevel.text = _building.Level.ToString();
    }
}