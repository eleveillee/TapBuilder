﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

// Displayed UI for the building
public class UIPanelInventory : MonoBehaviour {

    [SerializeField]
    GameObject _itemCellPrefab;

    public void Start()
    {
        IniVisual();
    }

    // Reinstantiate all cell. To be updated with a better system
    public void IniVisual()
    {
        int counter = 0;
        foreach (KeyValuePair<string, int> item in Portal.Instance.Inventory.ItemList)
        {
            DataItem data = GameManager.Instance.GameDefinition.GetItemById(item.Key);
            if (data == null)
                continue;

            GameObject go = (GameObject)Instantiate(_itemCellPrefab);
            go.transform.SetParent(transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = new Vector3(-885f, 60 + -80f * counter, 0f); // Hardcoded values should be replaced to something more dynamic


            UIItemCell uiCell = go.GetComponent<UIItemCell>();
            uiCell.Ini(data);
            counter++;
        }
    }
}