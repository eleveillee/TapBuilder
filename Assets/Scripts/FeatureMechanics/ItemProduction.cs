﻿using UnityEngine;
using System.Collections;

public class ItemProduction : Feature
{
    private DataItemProduction _data;

    private float _currentStorage;
    private float _timeLastProduction;

    private int itemCounter = 0;

	// Use this for initialization
	void Start ()
	{
        _data = (DataItemProduction)_dataFeature;
        _timeLastProduction = Time.time;
	}
	
	// Update is called once per frame
	protected override void TickUpdate()
	{
	    if (_timeLastProduction + _data.CoolDown <= Time.time)
        {
            Produce();
	    }

	}

    void Produce()
    {
        _timeLastProduction = Time.time;
        Item item = ItemSpawner.Instance.SpawnItem(_data.ItemId, transform.position + new Vector3(0.0f, -50.0f) + 10 * itemCounter * Vector3.down);
        _building.AddItem(item);
        itemCounter++;
    }

    /*public void StoreElectricty(float value)
    {
        m_currentStorage = Mathf.Max(0.0f, (Mathf.Min(m_customData.MaxStorage, m_currentStorage + value)));
        Debug.Log(m_currentStorage + "/" + m_customData.MaxStorage);
    }*/
}
