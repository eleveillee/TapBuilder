﻿using UnityEngine;
using System.Collections;

public class ItemCrafting : Feature
{
    private DataItemCrafting _data;

    private float m_currentStorage;
    private float m_timeLastCrafting;
    private bool _isBusy;

    int itemCounter;
	// Use this for initialization
	void Start ()
	{
        _data = (DataItemCrafting)_dataFeature;
	    m_timeLastCrafting = Time.time;
	    _isBusy = false;

	}
	
	// Update is called once per frame
	protected override void TickUpdate () {

        if (!_isBusy && m_timeLastCrafting + _data.Time <= Time.time && Portal.Instance.Inventory.ItemList[_data.ItemInput] >= 1)
        {
            PrepareCraft();
        }
    }

    void PrepareCraft()
    {
        _isBusy = true;
        Item neededItem = ItemSpawner.Instance.SpawnItem(_data.ItemInput, BuildingManager.Instance.GetBuilding(Database.BUILDING_PORTAL).transform.position + new Vector3(80.0f, -50.0f));
        neededItem.GoTo(transform.position, OnItemReceived);
        //Portal.Instance.Inventory.AddItem(_data.ItemInput, -1);
    }
        
    void Craft()
    {
        m_timeLastCrafting = Time.time; //Todo : Account for lost time or use TickUpdate
        ItemSpawner.Instance.SpawnItem(_data.ItemOutput, transform.position + new Vector3(0.0f, -50.0f) + 10 * itemCounter * Vector3.down);
        itemCounter++;
        _isBusy = false;
    }

    void OnItemReceived(Item item)
    {
        Craft();
        Destroy(item.gameObject);
    }

    /*public void StoreElectricty(float value)
    {
        m_currentStorage = Mathf.Max(0.0f, (Mathf.Min(m_customData.MaxStorage, m_currentStorage + value)));
        Debug.Log(m_currentStorage + "/" + m_customData.MaxStorage);
    }*/
}
