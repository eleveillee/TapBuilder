﻿using UnityEngine;
using System.Collections;

public class PowerGenerator : Feature
{
    private DataPowerGenerator _data;
    
	// Use this for initialization
	void Start ()
	{
        _data = (DataPowerGenerator)_dataFeature;
	    Power.UpdateGenerators(this, true);
	}
	
	// Update is called once per frame
	void Update ()
	{
	    //if (_building == null)
	      //  return;
          
	}

    void OnDestroy()
    {
        Power.UpdateGenerators(this, false);
    }
    
}
