﻿using UnityEngine;
using System.Collections;

public class Defence : Feature
{
    private DataDefence _data;

    private float _tapTime;
    void Awake()
    {

    }

	// Use this for initialization
	void Start ()
	{
        _data = (DataDefence)_dataFeature;
	}

    protected override void TickUpdate()
    {
        //if (Time.time - _tapTime > _data.Cooldown)
          //  Tap();
    }
	
	// Update is called once per frame
	void Tap ()
	{
	    MonsterAI ai = MonsterManager.Instance.GetRandomMonster();

	    if (ai == null)
	        return;

	    ai.Tap();
        _tapTime = Time.time;
    }
}
