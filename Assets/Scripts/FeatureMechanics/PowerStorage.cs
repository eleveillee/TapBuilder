﻿using UnityEngine;
using System.Collections;

public class PowerStorage : Feature
{
    private DataPowerStorage _customData;

    //private float _currentStorage;

	// Use this for initialization
	void Start ()
	{
	    _customData = (DataPowerStorage)_dataFeature;
        Power.UpdateStorages(this, true);

    }
	
	// Update is called once per frame
    void Update()
    {
    }

    void OnDestroy()
    {
        Power.UpdateStorages(this, false);
    }
}
