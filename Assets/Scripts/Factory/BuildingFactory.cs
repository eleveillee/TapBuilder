﻿using System;
using System.Collections.Generic;
using UnityEngine;


public static class MyExtensions
{
    public static void AddFeature(this Building building, GameObject go, DataFeature data)
    {
        Feature feature;
        switch (data.FeatureType)
        {
            case FeatureTypeEnum.Portal:
                feature = go.AddComponent<Portal>();
                break;
            case FeatureTypeEnum.PowerGenerator:
                feature = go.AddComponent<PowerGenerator>();
                break;
            case FeatureTypeEnum.PowerStorage:
                feature = go.AddComponent<PowerStorage>();
                break;
            case FeatureTypeEnum.Communication:
                feature = go.AddComponent<Communication>();
                break;
            case FeatureTypeEnum.ItemCrafting:
                feature = go.AddComponent<ItemCrafting>();
                break;
            case FeatureTypeEnum.ItemProduction:
                feature = go.AddComponent<ItemProduction>();
                break;
            case FeatureTypeEnum.Defence:
                feature = go.AddComponent<Defence>();
                break;
            default:
                Debug.LogError("FEATURE NOT FOUND");
                return;
        }

        if (feature != null)
        {
            feature.Data = data;
            feature.Building = building;
            building.Features.Add(data, feature);
        }
            
    }
}



/*public Feature GetFeature(Data data)
 {
     switch(data.FeatureType)
     {
         case FeatureTypeEnum.PowerGenerator:
             return new PowerGenerator();
         case FeatureTypeEnum.PowerStorage:
             return new PowerStorage();
     }

     return null;
 }*/
